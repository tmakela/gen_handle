///////////////////////////////////////////////////////////////////
//                                                               //
// Class structure for storing Pythia6 particle data into trees. //
//                                                               //
// Modes of operation:                                           //
//                                                               //
//    0: Generic events                                          //
//       -Normal QCD production for general studies              //
//                                                               //
//    1: Dijet events                                            //
//       -Normal QCD production with dijet-specific settings     //
//                                                               //
//    2: Photon+Jet events                                       //
//       - Photon+Jet production                                 //
//                                                               //
//    3: Zmumu+Jet events                                        //
//       - Z+Jet production with a Z -> mumu setting             //
//                                                               //
//    4: Ttbarlepton+jet events                                  //
//       - Ttbar production with WW -> qqbarllbar                //
//                                                               //
// Author: Hannu Siikonen (errai- @GitHub)                       //
// Last modification: 19.11.2015                                 //
//                                                               //
///////////////////////////////////////////////////////////////////

#ifndef PYTHIA6TREE
#define PYTHIA6TREE

/* Stdlib header file for input and output. */
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <stdexcept>

/* ROOT */
#include "TROOT.h"
#include "TFile.h"
#include "TBranch.h"
#include "TLorentzVector.h"
#include "TPythia6.h"
#include "TError.h"
#include "TTree.h"
#include "TClonesArray.h"
#include <TChain.h>
#include <TMatrix.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>

#include <TVector2.h>

// FastJet interface
#include "fastjet/config.h"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/Selector.hh"
#include "fastjet/tools/GridMedianBackgroundEstimator.hh"
#include "Pythia8Plugins/FastJet3.h"

/* scripts */
#include "../generic/help_functions.h"
#include "../events/PrtclEvent.h"

using std::string;
using std::vector;
using std::pair;
using std::cout;
using std::endl;
using std::cerr;
using std::runtime_error;
using std::stoi;
using std::map;

struct PartonHolder {
    fastjet::PseudoJet p4;
    int id;
    char tag;
    bool used;
};

class Pythia6Jets
{
public:
    /* The event loop should be called once after a proper initialization */
    void EventLoop();

    /* No copying or constructing with another instance */
    Pythia6Jets( const Pythia6Jets& other ) = delete;
    Pythia6Jets& operator=( const Pythia6Jets& ) = delete;

    /* Run settings are provided via the initializer */
    Pythia6Jets(Int_t nEvent, string fileName, Int_t nameId, const int mode);
    Pythia6Jets() :
        mInitialized(false)
    {
        cerr << "Pythia8Tree is intended to be used only with the non-default initializer" << endl;
    }
    /* ROOT has an awful behaviour with pointers. It should do the cleaning-up
       and such stuff well, but there is a ton of memory leaks that result
       already from ROOT 'being there'. At least the software runs - but the
       memory leaks and ROOT style pointer handling are regrettable. */
    ~Pythia6Jets() {}

protected:

    /* A handle for adding particle information */
    void                ParticleAdd(unsigned prt, char jetid);
    void                PartonAdd(unsigned prt, char jetid, char tag);
    void                PartonAdd(fastjet::PseudoJet prt, int pdgid, char jetid, char tag);
    void                JetAdd(unsigned jet);

    void                PartonAppend(fastjet::PseudoJet p4, unsigned prt, int tag);
    /* Particles needed by the hadronic flavor definition */
    void                GhostHadronAdd(unsigned prt, bool useStrange = false);

    /* Loop over particles within an event: return true if event is to be saved */
    bool                ParticleLoop();
    /* The logic within particleloop */
    bool                ProcessParticle(unsigned prt);
    /* Allow a event type based veto */
    bool                Veto();
    /* Jet clustering routines */
    bool                JetLoop();
    /* Isolation for e.g. photons */
    bool                Isolation();

    /* Settings that depend on the selected mode */
    void                ModeSettings();
    /* General settings that are always used */
    void                GeneralSettings();

    /* A handle for adding a hard process photon descended from the signal event photon */
    bool                GammaAdd();
    /* A handle for adding the two muons originating from a hard process Z prt */
    bool                MuonAdd();
    /* A handle for adding the produced leptons in ttbar events */
    bool                LeptonAdd(unsigned prt);

    /* See: HadronAndPartonSelector.cc in CMSSW. Indicates whether a ghost hadron
     * is in an excited state or not. Checks whether a hadron has a daughter of
     * the same flavour. Parameter quarkId is a PDG quark flavour. */
    bool                IsExcitedHadronState(unsigned prt, int quarkId);
    /* A function that checks whether a photon is originated from a pi0 and that
     * the energy of the photon-pair corresponds to the pion. returns 0 if
     * the origin is not a pion with good energy and 1 if it is */
    bool                GammaChecker(unsigned);
    /* Has the particle already been appended */
    bool                Absent(unsigned prt);

    /* Print the decay tree */
    void                PrintParticle(unsigned prt);
    void                PrintEvent();

    /* A handy handle */
    TLorentzVector      TLorentzify(unsigned prt);
    /* Another handy handle */
    fastjet::PseudoJet  PseudoJettify(unsigned prt);
    fastjet::PseudoJet  PseudoJettify(TLorentzVector p4);

    /* Corrected parton momentum */
    unsigned            OptimalParton(unsigned prt);
    TLorentzVector      LastParton(unsigned prt,bool &stop);
    TLorentzVector      NextParton(unsigned prt,unsigned lvl);

    /* Cuts for jet property calculations */
    void                Cuts();
    /* Fragmentation function calculation */
    double              PTD();
    /* Calculation of jet inner radius */
    double              Sigma2();
    /* Helper functions for Cuts */
    bool                IsCharged(int pdgid);
    bool                IsHadron(int pdgid);
    /* A shortcut for finding out what stuff is partons (no beam remnants) */
    bool                            IsParton(unsigned prt);
    /* Check whether this is an FS parton */
    bool                            IsFSParton(unsigned prt);

protected:

    /* Indicator that the event loop can be run */
    bool                            mInitialized;
    /* A general-purpose counter for physics debugging */
    unsigned                        mCounter;
    unsigned                        mNumErrs;

    unsigned                        mHardProcCount;

    TPythia6                       *mPythia;

    TFile                          *mFile;
    TTree                          *mTree;
    TBranch                        *mBranch;

    int                             mNumEvents;
    int                             mMode;
    int                             mTimerStep;
    Timer                           mTimer;

    vector<unsigned>                mSpecialIndices;
    map<unsigned,TLorentzVector>    mPartonHistory;

    float                 mWeight;
    /* Particle level. */
    vector<unsigned char> mJetId;
    vector<int>           mPDGID;
    vector<float>         mPt;
    vector<float>         mEta;
    vector<float>         mPhi;
    vector<float>         mE;
    /* Parton level. */
    vector<char>          mPJetId;
    vector<int>           mPPDGID;
    vector<char>          mPTag;
    vector<float>         mPPt;
    vector<float>         mPEta;
    vector<float>         mPPhi;
    vector<float>         mPE;
    vector<float>         mDR;
    /* Jet level. */
    vector<float>         mJPt;
    vector<float>         mJEta;
    vector<float>         mJPhi;
    vector<float>         mJE;
    vector<int>           mConst;
    vector<float>         mPTD;
    vector<float>         mSigma2;
    /* MET level. */
    float                 mMet;
    fastjet::PseudoJet    mMetVect;
    fastjet::PseudoJet gamma;

    double                     mGhostCoeff;
    vector<PartonHolder>       mPartonInfo;

    fastjet::JetDefinition     mJetDef;
    vector<fastjet::PseudoJet> mJetInputs;
    vector<fastjet::PseudoJet> mSortedJets;
    vector<fastjet::PseudoJet> mJetParts;
    vector<fastjet::PseudoJet> mCutJetParts;
    vector<TLorentzVector>     mPartons;

    int                             mEnergy;
    int                             mTune;
    bool                            mMPI;
    bool                            mISR;
    bool                            mFSR;
    bool                            mUseStrange;

    static const unsigned           mNumSeeds = 40;
    const unsigned                  mSeeds[mNumSeeds] = {840744607,
                                                         431166825,
                                                         11489507,
                                                         859341684,
                                                         719632152,
                                                         384411333,
                                                         90405435,
                                                         297596781,
                                                         620424940,
                                                         829585206,
                                                         350220548,
                                                         862060943,
                                                         865146589,
                                                         11119376,
                                                         706126850,
                                                         761335296,
                                                         286390445,
                                                         408256820,
                                                         447625541,
                                                         368022699,
                                                         281922559,
                                                         852542479,
                                                         509348179,
                                                         175162098,
                                                         688006297,
                                                         512118632,
                                                         676751467,
                                                         212155085,
                                                         158795947,
                                                         68988051,
                                                         258456879,
                                                         625579469,
                                                         146828216,
                                                         582720998,
                                                         226158642,
                                                         439232438,
                                                         366169042,
                                                         745702146,
                                                         412672564,
                                                         177882235};
};

#endif // PYTHIA6TREE
