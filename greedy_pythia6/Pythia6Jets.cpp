#include "Pythia6Jets.h"

/* HOX: Tevatron setup */

Pythia6Jets::Pythia6Jets(Int_t nEvent, string fileName, Int_t nameId, const int mode) :
    mMode(mode),
    mNumEvents(nEvent),
    mInitialized(true),
    mEnergy(1960),
    mTune(2),
    mMPI(true),
    mISR(true),
    mFSR(true),
    mUseStrange(false),
    mCounter(0),
    mGhostCoeff(pow(10,-18))
{
    /* Create an instance of the Pythia event generator: */
    mPythia = new TPythia6;
    /* Set a seed value according to the run index and make sure it is used: */
    if ( nameId < 1 || nameId > 10 )
        throw std::runtime_error("Incompatible seed ID");
    mPythia->SetMRPY(1,mSeeds[nameId-1]);
    mPythia->SetMRPY(2,0);

    /* Event type: */
    ModeSettings();
    /* Other settings: */
    GeneralSettings();

    /* Try to create a file to write */
    mFile = TFile::Open(fileName.c_str(), "RECREATE");
    if(!mFile->IsOpen()) throw runtime_error("Creating an output file failed");
    mFile->SetCompressionLevel(1);

    /* Output tree: */
    mTree = new TTree("Pythia6Jets", "Pythia6 particle data.");
    if(!mTree) throw runtime_error("Creating a tree failed");
    mTree->SetAutoSave(100000000); /* 0.1 GBytes */
    mTree->SetCacheSize(10000000); /* 100 MBytes */
    TTree::SetBranchStyle(1); /* New branch style */

    mTree->Branch("weight",&mWeight)->SetAutoDelete(kFALSE);
    /* Particle lvl */
    mTree->Branch("prtcl_jet",&mJetId)->SetAutoDelete(kFALSE);
    mTree->Branch("prtcl_pdgid",&mPDGID)->SetAutoDelete(kFALSE);
    mTree->Branch("prtcl_pt",&mPt)->SetAutoDelete(kFALSE);
    mTree->Branch("prtcl_eta",&mEta)->SetAutoDelete(kFALSE);
    mTree->Branch("prtcl_phi",&mPhi)->SetAutoDelete(kFALSE);
    mTree->Branch("prtcl_e",&mE)->SetAutoDelete(kFALSE);
    /* Parton lvl */
    mTree->Branch("prtn_jet",&mPJetId)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_pdgid",&mPPDGID)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_tag",&mPTag)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_pt",&mPPt)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_eta",&mPEta)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_phi",&mPPhi)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_e",&mPE)->SetAutoDelete(kFALSE);
    mTree->Branch("prtn_dr",&mDR)->SetAutoDelete(kFALSE);
    /* Jet lvl */
    mTree->Branch("jet_pt",&mJPt)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_eta",&mJEta)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_phi",&mJPhi)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_e",&mJE)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_constituents",&mConst)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_ptd",&mPTD)->SetAutoDelete(kFALSE);
    mTree->Branch("jet_sigma2",&mSigma2)->SetAutoDelete(kFALSE);

    mTree->Branch("met",&mMet)->SetAutoDelete(kFALSE);

    mTree->BranchRef();

    mJetDef = fastjet::JetDefinition(fastjet::antikt_algorithm, 0.4, fastjet::E_scheme, fastjet::Best);
    /* Setup a custom event timer */
    mTimerStep = 1000;
    mTimer.setParams(mNumEvents,mTimerStep);
    mTimer.startTiming();
} // Pythia6Jets


void Pythia6Jets::ModeSettings() {
    cerr << "Initializing Pythia6 ..." << endl;
    if (mMode == 1 || mMode == 0) {
        cerr << "  Generating standard QCD events." << endl;
        mPythia->SetMSEL(1);
        // Min and max pthat
        mPythia->SetCKIN(3,20);
        mPythia->SetCKIN(4,3000);
    } else if (mMode == 2) {
        cerr << "  Generating photon+jet events." << endl;
        //pythia->SetMSEL(10); //(includes gamma-gamma bkg)
        mPythia->SetMSEL(0);
        mPythia->SetMSUB(14,1);
        mPythia->SetMSUB(29,1);
        mPythia->SetMSUB(115,1);
        // Min and max pthat
        mPythia->SetCKIN(3,20);
        mPythia->SetCKIN(4,3000);
    } else if (mMode == 3) {
        cerr << "  Generating Zmumu+jet events." << endl;
        mPythia->SetMSEL(13); // 11 would be the vanilla y*/Z
        // Leave only decay to muons on
        mPythia->SetMDME(174,1,0); // Z decay to d dbar
        mPythia->SetMDME(175,1,0); // Z decay to u ubar
        mPythia->SetMDME(176,1,0); // Z decay to s sbar
        mPythia->SetMDME(177,1,0); // Z decay to c cbar
        mPythia->SetMDME(178,1,0); // Z decay to b bbar
        mPythia->SetMDME(179,1,0); // Z decay to t tbar
        mPythia->SetMDME(182,1,0); // Zee
        mPythia->SetMDME(183,1,0); // Znuenue
        mPythia->SetMDME(184,1,1); // Zmumu
        mPythia->SetMDME(185,1,0); // Znumunumu
        mPythia->SetMDME(186,1,0); // Ztautau
        mPythia->SetMDME(187,1,0); // Znutaunutau
        // Min and max mhat
        mPythia->SetCKIN(1,40);
        mPythia->SetCKIN(2,-1);
        // Min and max pthat
        mPythia->SetCKIN(3,20);
        mPythia->SetCKIN(4,3000);
    } else if (mMode == 4) {
        cerr << "  Generating ttbar lepton+jets events." << endl;
        mPythia->SetMSEL(6); // choose top quark
        mPythia->SetMSUB(81,1); // qqbar -> qqbar
        mPythia->SetMSUB(82,1); // gg->qqbar
        mPythia->SetPMAS(6,1,172);
        mPythia->SetCKIN(3,25);
        mPythia->SetCKIN(4,3000);
    } else {
        throw std::runtime_error("The selected mode is nonsense");
    }
} // ModeSettings


void Pythia6Jets::GeneralSettings() {
    mPythia->SetMSTU(21,2); // Check for errors (the hard way! - default)
    mPythia->SetMSTJ(22,2); // Unstable particle decay: (according to lifetime)
    mPythia->SetPARJ(71,10); // ctau = 10 mm
    mPythia->SetMSTP(33,0); // no K factors in hard cross sections (this is the default value)
    mPythia->SetMSTP(2,1); // which order running alphaS (this is also the default value)
    mPythia->SetMSTP(51,10042); // Structure function (PDF CTEQ6L1)
    mPythia->SetMSTP(52,2); // LHAPDF

    mPythia->SetMSTP(142,2); // Turn on Pt reweighting

    if (mTune==0) {
        /* Z2*, a classic CMS tune */
        cerr << "  Z2* tune has been selected." << endl;
        mPythia->SetPARP(82,1.921); // pt cutoff, multiparton interactions
        mPythia->SetPARP(89,1800.); // sqrts for which parp82 is set
        mPythia->SetPARP(90,0.227); // MPI: rescaling power

        mPythia->SetMSTP(95,6); // Color reconnection setParams
        mPythia->SetPARP(77,1.016); // CR
        mPythia->SetPARP(78,0.538); // CR

        mPythia->SetPARP(80,0.1); // Prob. colored parton from BBR

        mPythia->SetPARP(83,0.356); // MPI matter distribution
        mPythia->SetPARP(84,0.651); // MPI matter distribution

        mPythia->SetPARP(62,1.025); // ISR cutoff

        mPythia->SetMSTP(91,1); // Gaussian primordial KT
        mPythia->SetMSTP(93,10.0); // Primordial KT-max

        mPythia->SetMSTP(81,21); // MPI
        mPythia->SetMSTP(82,4); // MPI model
    } else if (mTune==1) {
        /* cuep6s1 tune by CMS, the newest but not used that much */
        cerr << "  CUEP6S1 tune has been selected." << endl;
        mPythia->SetPARP(82,1.9096); // pt cutoff, multiparton interactions
        mPythia->SetPARP(89,1800.); // sqrts for which parp82 is set
        mPythia->SetPARP(90,0.2479); // MPI: rescaling power

        mPythia->SetMSTP(95,6); // Color reconnection setParams
        mPythia->SetPARP(77,0.6646); // CR
        mPythia->SetPARP(78,0.5454); // CR

        mPythia->SetPARP(80,0.1); // Prob. colored parton from BBR

        mPythia->SetPARP(83,0.8217); // MPI matter distribution
        mPythia->SetPARP(84,0.651); // MPI matter distribution

        mPythia->SetPARP(62,1.025); // ISR cutoff

        mPythia->SetMSTP(91,1); // Gaussian primordial KT
        mPythia->SetMSTP(93,10.0); // Primordial KT-max

        mPythia->SetMSTP(81,21); // MPI
        mPythia->SetMSTP(82,4); // MPI model

        mPythia->SetPARJ(1,0.08); // HAD diquark suppression
        mPythia->SetPARJ(2,0.21); // HAD strangeness suppression
        mPythia->SetPARJ(3,0.94); // HAD strange diquark suppression
        mPythia->SetPARJ(4,0.04); // HAD vectior diquark suppression
        mPythia->SetPARJ(11,0.35); // HAD P(vector meson), u and d only
        mPythia->SetPARJ(12,0.35); // HAD P(vector meson) contains
        mPythia->SetPARJ(13,0.54); // HAD P(vector meson), heavy quarks
        mPythia->SetPARJ(21,0.34); // HAD fragmentation pt
        mPythia->SetPARJ(25,0.63); // HAD eta0 suppression
        mPythia->SetPARJ(26,0.12); // HAD eta0 suppression
    } else if (mTune==2) {
        mPythia->SetMSTP(5,100); // Tune A, heaviliy used at the tevatron
    }

    if (!mISR) {
        mPythia->SetMSTP(61,0);
        cerr << "  WARNING: ISR has been turned off!" << endl;
    }
    if (!mFSR) {
        mPythia->SetMSTP(71,0); // FSR off
        cerr << "  WARNING: FSR has been turned off!" << endl;
    }
    if (!mMPI) {
        mPythia->SetMSTP(81,0); // MPI off
        cerr << "  WARNING: MPI has been turned off!" << endl;
    }

    cerr << "  CMS energy of " << mEnergy/1000 << " TeV has been chosen." << endl;
    mPythia->Initialize("cms", "p", "pbar", mEnergy);
} // GeneralSettings


// mPythia->Pylist(2);
// mPythia->Pystat(1);
void Pythia6Jets::EventLoop()
{
    if (!mInitialized) {
        cerr << "Event loop can be performed only after proper initialization" << endl;
        return;
    }

    /* The event loop */
    unsigned evtNo = 0;
    // mPYthia->Pylist(2); for printing an event
    while (evtNo != mNumEvents) {
        mPythia->GenerateEvent();
        if (!ParticleLoop()) continue;
        if (Veto()) continue;
        if (!JetLoop()) continue;

        mTree->Fill();

        ++evtNo;
        if (evtNo%mTimerStep==0) mTimer.printTime();
    }

    mFile = mTree->GetCurrentFile();
    mTree->AutoSave("Overwrite");
    mFile->Close();

    if (mCounter != 0)
        cerr << "Non-zero counter value: " << mCounter << endl;

    delete mPythia;
    mPythia = 0;
    mInitialized = false;
} // EventLoop


bool Pythia6Jets::ParticleLoop()
{
    /* Particle lvl */
    mJetId.clear();
    mPDGID.clear();
    mPt.clear();
    mEta.clear();
    mPhi.clear();
    mE.clear();
    /* Parton lvl */
    mPJetId.clear();
    mPPDGID.clear();
    mPTag.clear();
    mPPt.clear();
    mPEta.clear();
    mPPhi.clear();
    mPE.clear();
    mDR.clear();
    /* Jet lvl */
    mJPt.clear();
    mJEta.clear();
    mJPhi.clear();
    mJE.clear();
    mConst.clear();
    mPTD.clear();
    mSigma2.clear();

    mJetInputs.clear();
    mSortedJets.clear();

    mPartonInfo.clear();

    mPartonHistory.clear();
    /* Special particle indices are saved to eliminate saving overlap. */
    mSpecialIndices.clear();

    /* Pythia 6 speciality: do the gamma/muon analysis before the particle loop. */
    if (mMode==2)
        GammaAdd();
    else if (mMode==3)
        MuonAdd();
    else if (mMode==4) {
        unsigned partno = 0, leptno = 0;
        for (unsigned prt = 13; prt <= 16; ++prt) {
            unsigned id = abs(mPythia->GetK(prt,2));

            if (IsParton(prt)) {
                PartonAppend(PseudoJettify(prt),prt,0);
                ++partno;
                mPartonHistory.emplace(prt,TLorentzVector());
            } else if (id < 20) {
                PartonAdd(prt,-1,2);
                ++leptno;
            }
        }

        if (partno!=2 || leptno!=2 || partno%2!=0 || leptno%2!=0 || (partno+leptno)!=4)
            return false;
    }

    mWeight = 1./mPythia->GetVINT(99);

    mMetVect = fastjet::PseudoJet();
    mHardProcCount = 0;
    mNumErrs = 0;

    /* Particle loop */
    for (unsigned prt = 1; prt <= mPythia->GetN(); ++prt)
        if (!ProcessParticle(prt))
            return false;
    mMet = mMetVect.perp();

    /* Adding corrected parton momenta */
    for (auto prt : mPartonHistory) {
        PartonAppend(PseudoJettify(prt.second),prt.first,1);
    }

    return true;
} // ParticleLoop

/* Adding final-state particles associated with jets */
void Pythia6Jets::ParticleAdd(unsigned prt, char jetid)
{
    int pdgid = mPythia->GetK(prt,2);
    if (pdgid==22 && GammaChecker(prt)) pdgid = 20;
    TLorentzVector handle = TLorentzify(prt);

    mJetId.push_back(jetid);
    mPDGID.push_back(pdgid);
    mPt.push_back(handle.Pt());
    mEta.push_back(handle.Eta());
    mPhi.push_back(handle.Phi());
    mE.push_back(handle.E());
} // ParticleAdd

void Pythia6Jets::PartonAdd(unsigned prt, char jetid, char tag)
{
    TLorentzVector handle = TLorentzify(prt);

    mPJetId.push_back(jetid);
    mPTag.push_back(tag);
    mPPDGID.push_back(mPythia->GetK(prt,2));
    mPt.push_back(handle.Pt());
    mEta.push_back(handle.Eta());
    mPhi.push_back(handle.Phi());
    mE.push_back(handle.E());
} // PartonAdd

void Pythia6Jets::PartonAdd(fastjet::PseudoJet prt, int pdgid, char jetid, char tag)
{
    mPJetId.push_back(jetid);
    mPPDGID.push_back(pdgid);
    mPTag.push_back(tag);
    mPPt.push_back(prt.pt());
    mPEta.push_back(prt.eta());
    mPPhi.push_back(prt.phi());
    mPE.push_back(prt.e());
    mDR.push_back(jetid<0 ? -1.0 : prt.delta_R(mSortedJets[jetid]));
} // PartonAdd

void Pythia6Jets::JetAdd(unsigned jet)
{
    mJPt.push_back(mSortedJets[jet].pt());
    mJEta.push_back(mSortedJets[jet].eta());
    mJPhi.push_back(mSortedJets[jet].phi());
    mJE.push_back(mSortedJets[jet].e());
    /* Start messing around with jet constituents */
    mJetParts = mSortedJets[jet].constituents();
    Cuts();
    mConst.push_back(mCutJetParts.size());
    mPTD.push_back(PTD());
    mSigma2.push_back(Sigma2());
} // JetAdd

bool Pythia6Jets::GammaAdd()
{
    // The ninth particle should be a photon (error, if not)
    unsigned prt = 9;
    while (mPythia->GetK(prt,2)!=22 && (mPythia->GetK(prt,3)==7||mPythia->GetK(prt,3)==8))
        ++prt;
    mSpecialIndices.push_back(prt);
    PartonAdd(mSpecialIndices[0], -1, 2);

    while (mPythia->GetK(mSpecialIndices[0],1)>10)
        mSpecialIndices[0] = mPythia->GetK(mSpecialIndices[0],4);

    if (mPythia->GetK(mSpecialIndices[0],2) != 22)
        throw std::logic_error("The photon finder did not find a photon.");

    PartonAdd(mSpecialIndices[0], -1, 3);
    gamma = PseudoJettify(mSpecialIndices[0]);
    ++mHardProcCount;

    return true;
} // GammaAdd

bool Pythia6Jets::MuonAdd()
{
    // The twelwth and fourteenth particles (or later) are muons
    mSpecialIndices.push_back(12); mSpecialIndices.push_back(13);
    while (abs(mPythia->GetK(mSpecialIndices[0],2))!=13) {
        ++mSpecialIndices[0];
        ++mSpecialIndices[1];
    }
    PartonAdd(mSpecialIndices[0], -1, 2);
    PartonAdd(mSpecialIndices[1], -1, 2);

    for (unsigned i = 0; i < mSpecialIndices.size(); ++i) {
        if (abs(mPythia->GetK(mSpecialIndices[i],2))!=13)
            throw std::logic_error("The muon finder did not find a muon.");

        while (mPythia->GetK(mSpecialIndices[i],1)>10) {
            for (unsigned probe = mPythia->GetK(mSpecialIndices[i],4);
                 probe <= mPythia->GetK(mSpecialIndices[i],5); ++probe) {
                if (abs(mPythia->GetK(probe,2))==13) {
                    mSpecialIndices[i] = probe;
                    break;
                }
            }
        }
        PartonAdd(mSpecialIndices[i], -1,3);
    }
    if (mSpecialIndices.size()==2) {
        ++mHardProcCount;
        return true;
    }

    cerr << "Failed to locate muon pair! " << ++mNumErrs << endl;
    return false;
} // MuonAdd

bool Pythia6Jets::LeptonAdd(unsigned prt)
{
    /* Save a raw lepton before separation to neutrinos and charged leptons */
    PartonAdd(prt, -1, 2);

    /* Charged lepton input: find a final-state charged lepton. */
    int type = abs(mPythia->GetK(prt,2))%2;
    if (type) {
        /* Charged leptons require processing. */
        while (mPythia->GetK(prt,1)>9) {
            unsigned tmpPrt = 0;
            for (unsigned i = mPythia->GetK(prt,4); i <= mPythia->GetK(prt,5); ++i) {
                unsigned dType = abs(mPythia->GetK(i,2))%2;
                unsigned dId = abs(mPythia->GetK(i,2));
                if ((dId>10 && dId < 20) && dType==1)
                    tmpPrt = i;
            }

            /* This occurs around 25-30% of the time originating from tau decay */
            if (tmpPrt == 0)
                return false; /* Charged lepton decays to partons and a neutrino */

            prt = tmpPrt;
        }

        if (abs(mPythia->GetK(prt,2))==15)
            throw std::logic_error("Final-state tau spotted.");
    }
    mSpecialIndices.push_back(prt);
    PartonAdd(prt, -1, 3);
    return true;
} // LeptonAdd


bool Pythia6Jets::IsExcitedHadronState(unsigned int prt, int quarkId)
{
    for (unsigned dtr = mPythia->GetK(prt,4), N = mPythia->GetK(prt,5); dtr <= N; ++dtr)
        if (HadrFuncs::StatusCheck(quarkId, mPythia->GetK(dtr,2)))
            return true;
    return false;
}


inline bool Pythia6Jets::Absent(unsigned int prt)
{
    return std::find(mSpecialIndices.begin(),mSpecialIndices.end(),prt)==mSpecialIndices.end();
}


///////////////////////////////////////////////////
// Event type specific ProcessParticle functions //
///////////////////////////////////////////////////

void Pythia6Jets::PartonAppend(fastjet::PseudoJet p4, unsigned prt, int tag)
{
    mPartonInfo.push_back(PartonHolder{p4, mPythia->GetK(prt,2), tag, false});
    p4 *= mGhostCoeff;
    p4.set_user_index(-mPartonInfo.size());
    mJetInputs.push_back(p4);
}


bool Pythia6Jets::ProcessParticle(unsigned prt)
{
    int status = mPythia->GetK(prt,1);
    int id = abs(mPythia->GetK(prt,2));

    // prt == 7,8: outgoing LO particles (hard process)
    if (mMode > 0 && id < 22) {
        unsigned parent = mPythia->GetK(prt,3);
        if (mMode < 4) {
            if (prt==7 || prt==8) {
                ++mHardProcCount;
                if (status!=21 || !IsParton(prt))
                    throw std::runtime_error("False assumptions in hard process.");

                mPartonHistory.emplace(prt,TLorentzVector());

                PartonAppend(PseudoJettify(prt),prt,0);
                return true;
            } else if (parent==7 || parent==8) {
                bool dummy;
                mPartonHistory[parent] += LastParton(prt,dummy);
                int tag = 2*(parent-5);
                unsigned truth = OptimalParton(prt);
                PartonAppend(PseudoJettify(truth),truth,tag);
                bool dimmy;
                PartonAppend(PseudoJettify(LastParton(truth,dimmy)),truth,tag+1);
            }
        } else if (mMode==4) {
            if (parent >= 13 && parent <= 16) {
                if (IsParton(prt)) {
                    bool dummy;
                    mPartonHistory[parent] += LastParton(prt,dummy);
                } else {
                    LeptonAdd(prt);
                }
            }
        }
    }

    /* Stable particles, check that this is no special case. */
    if (status <= 10 && Absent(prt)) {
        fastjet::PseudoJet part = PseudoJettify(prt);
        /* Set the user index to pdgid, except let this be 20 for pi0 photons */
        part.set_user_index(prt);
        mJetInputs.push_back(part);
        if (id==12||id==14||id==16)
            mMetVect += part;
    }

    return true;
} // ProcessParticle


bool Pythia6Jets::JetLoop()
{
    fastjet::ClusterSequence clustSeq(mJetInputs, mJetDef);
    vector<fastjet::PseudoJet> unsorted = clustSeq.inclusive_jets(10.0);
    mSortedJets = sorted_by_pt(unsorted);

    for (unsigned jet = 0; jet<mSortedJets.size(); ++jet) {
        JetAdd(jet);

        for (auto &part : mJetParts) {
            int prt = part.user_index();
            if (prt<0) {
                /* Adding uncorrected and corrected parton momentum */
                prt *= -1;
                --prt;
                PartonAdd(mPartonInfo[prt].p4,mPartonInfo[prt].id,jet,mPartonInfo[prt].tag);
                mPartonInfo[prt].used = true;
            } else {
                ParticleAdd(prt,jet);
            }
        }
    }

    for (unsigned prt = 0; prt < mPartonInfo.size(); ++prt) {
        if (mPartonInfo[prt].used)
            continue;
        PartonAdd(mPartonInfo[prt].p4,mPartonInfo[prt].id,-1,mPartonInfo[prt].tag);
    }

    return true;
} // JetLoop


inline bool Pythia6Jets::Veto()
{
    return !Isolation();
}

bool Pythia6Jets::Isolation()
{
    if (mMode == 2) {
        double limit = 0.1;
        double R = 0.1;
        double E_dR = 0;
        for (auto part : mJetInputs) {
            if (part.user_index()<0)
                continue;
            double dR = gamma.delta_R(part);
            if (dR < R)
                E_dR += part.e();
        }

        if (E_dR/gamma.e() > limit)
            return false;
        else
            return true;
    } else {
        return true;
    }
}


/* Does a photon originate from pions? */
bool Pythia6Jets::GammaChecker(unsigned prt)
{
    /* One mother, which is pi0? */
    int mother = mPythia->GetK(prt,3);
    if (!mother || abs(mPythia->GetK(mother,2))!=111) return false;

    int d1 = mPythia->GetK(mother,4);
    int d2 = mPythia->GetK(mother,5);
    double eDifference = mPythia->GetP(mother,4);
    for (int daugh = mPythia->GetK(mother,4), N = mPythia->GetK(mother,5); daugh <= N; ++daugh)
        eDifference -= mPythia->GetP(daugh,4);

    if (fabs(eDifference) > 0.001)
        return false;

    return true;
}


/* The most recent parton with more than one daughter */
unsigned int Pythia6Jets::OptimalParton(unsigned prt)
{
    unsigned idAbs = abs(mPythia->GetK(prt,2));
    if (!IsParton(prt) || IsFSParton(prt) || idAbs>69)
        return prt;

    while (mPythia->GetK(prt,4) == mPythia->GetK(prt,5) && mPythia->GetK(prt,4)!=0)
        prt = mPythia->GetK(prt,4);

    return prt;
}

/* Corrected parton momentum */
TLorentzVector Pythia6Jets::LastParton(unsigned prt, bool &prev_stop)
{
    unsigned idAbs = abs(mPythia->GetK(prt,2));
    if (!IsParton(prt) || IsFSParton(prt) || idAbs>69) {
        prev_stop = true;
        return TLorentzify(prt);
    }

    // stop: detect if even one of the daughters is beyond final state
    bool stop = false;
    TLorentzVector cumulator;
    for (unsigned i = mPythia->GetK(prt,4); i <= mPythia->GetK(prt,5); ++i) {
        cumulator += LastParton(i,stop);
        if (stop)
            return TLorentzify(prt);
    }

    return cumulator;
}

/* Corrected parton momentum (contrast to FS partons 71 and 72) */
TLorentzVector Pythia6Jets::NextParton(unsigned prt, unsigned lvl)
{
    if (!IsParton(prt) || IsFSParton(prt))
        return TLorentzify(prt);

    while (mPythia->GetK(prt,4) == mPythia->GetK(prt,5))
        prt = mPythia->GetK(prt,4);

    if (lvl==0)
        return TLorentzify(prt);

    TLorentzVector cumulator;
    for (unsigned daughter = mPythia->GetK(prt,4); daughter <= mPythia->GetK(prt,5); ++daughter) {
        if (!IsParton(prt) || IsFSParton(prt)) {
            cumulator += NextParton(daughter,lvl-1);
        } else {
            return TLorentzify(prt);
        }
    }

    return cumulator;
}


/* A very handy handle */
inline TLorentzVector Pythia6Jets::TLorentzify(unsigned int prt)
{
    return TLorentzVector(mPythia->GetP(prt,1),mPythia->GetP(prt,2),mPythia->GetP(prt,3),mPythia->GetP(prt,4));
}

/* Another handy handle */
inline fastjet::PseudoJet Pythia6Jets::PseudoJettify(unsigned int prt)
{
    return fastjet::PseudoJet(mPythia->GetP(prt,1),mPythia->GetP(prt,2),mPythia->GetP(prt,3),mPythia->GetP(prt,4));
}

inline fastjet::PseudoJet Pythia6Jets::PseudoJettify(TLorentzVector p4) {
    return fastjet::PseudoJet(p4.Px(),p4.Py(),p4.Pz(),p4.E());
}


double Pythia6Jets::PTD()
{
    double square = 0, linear = 0;
    for(size_t q = 0; q != mCutJetParts.size(); ++q) {
        square += pow(mCutJetParts[q].pt(),2);
        linear += mCutJetParts[q].pt();
    }
    return sqrt(square)/linear;
}

double Pythia6Jets::Sigma2()
{
    double weightedDiffs[4] = {0,0,0,0};
    double phi = 0, eta = 0, pT2 = 0;

    for(size_t q = 0; q != mCutJetParts.size(); ++q) {
        pT2 += pow(mCutJetParts[q].pt(),2);
        eta += pow(mCutJetParts[q].pt(),2)*mCutJetParts[q].eta();
        phi += pow(mCutJetParts[q].pt(),2)*mCutJetParts[q].phi();
    }
    eta /= pT2; phi /= pT2;

    for(unsigned int q = 0; q != mCutJetParts.size(); ++q)
    {
        double deltaEta = eta-mCutJetParts[q].eta();
        double deltaPhi = TVector2::Phi_mpi_pi(phi-mCutJetParts[q].phi());
        double pT2Tmp = pow(mCutJetParts[q].pt(),2);
        weightedDiffs[0] += pT2Tmp*deltaEta*deltaEta;
        weightedDiffs[3] += pT2Tmp*deltaPhi*deltaPhi;
        weightedDiffs[1] -= pT2Tmp*deltaEta*deltaPhi;
    }
    weightedDiffs[2] = weightedDiffs[1];

    TMatrixDSymEigen me( TMatrixDSym(2,weightedDiffs) );
    TVectorD eigenvals = me.GetEigenValues();

    return sqrt(eigenvals[1]/pT2);
}

void Pythia6Jets::Cuts()
{
    mCutJetParts.clear();
    bool cutMode = true;

    if (cutMode) {
        /* Explicit cuts (pt cut for photons and neutral hadrons) */
        vector<fastjet::PseudoJet> tmpParts;
        for (auto &q : mJetParts) {
            if (q.user_index() < 0)
                continue;
            int id = abs(mPythia->GetK(q.user_index(),2));
            if (!( q.pt()<1 && (id == 22 || (IsHadron(id) && !IsCharged(id)))) )
                tmpParts.push_back(q);
        }

        /* Implicit cuts (pt cut for hadrons) */
        for (auto &q : tmpParts) {
            int id = abs(mPythia->GetK(q.user_index(),2));
            if ( !IsHadron(id) || ( (IsCharged(id) && q.pt()>0.3) || (!IsCharged(id) && q.pt()>3) ) )
                mCutJetParts.push_back( q );
        }
    } else {
        for (auto &q : mJetParts) {
            if (q.user_index() < 0)
                continue;
            mCutJetParts.push_back(q);
        }
    }
}


bool Pythia6Jets::IsHadron(int pdg)
{
    if(pdg>99) return true;
    return false;
}

bool Pythia6Jets::IsCharged(int pdg)
{
    int charge = 0;
    /* photons and neutrinos */
    if (pdg==22 || pdg==12 || pdg==14 ||pdg==16 ) return false;
    /* charged leptons */
    if (pdg==11 || pdg==13 || pdg==15 ) return true;

    pdg = (pdg/10)%1000;
    if (pdg < 100) { /* Mesons */
        if ((pdg%10)%2 == 0) { charge += 2; }
        else { charge -= 1; }

        if ((pdg/10)%2 == 0) { charge -= 2; }
        else { charge += 1; }

    } else { /* Baryons */
        while (pdg != 0) {
            int digit = pdg%10;
            pdg = pdg/10;
            if (digit%2 == 0) { charge += 2; }
            else { charge -= 1; }
        }
    }
    if (charge == 0) return false;
    else return true;
}

inline bool Pythia6Jets::IsParton(unsigned int prt)
{
    unsigned id = abs(mPythia->GetK(prt,2));
    return (id <= 6 || id == 21);
}

/* This assumes implicitly that the particle prt is a parton */
inline bool Pythia6Jets::IsFSParton(unsigned int prt)
{
    /* No decays in sight */
    if (mPythia->GetK(prt,4)>mPythia->GetK(prt,5))
        return false;

    /* The parton has a parton daughter */
    for (unsigned dtr = mPythia->GetK(prt,4), N = mPythia->GetK(prt,5); dtr <= N; ++dtr) {
        unsigned id = abs(mPythia->GetK(prt,2));
        if (id > 90 && id < 94)
            return false;
    }

    return true;
}


/* Graphviz printing for a single particle */
void Pythia6Jets::PrintParticle(unsigned prt)
{
    unsigned stat = mPythia->GetK(prt,2);
    unsigned mother = mPythia->GetK(prt,3);
    if (mother == 0 && prt > 2)
        mother = prt-2;
    cout << mother << " -> " << prt << " [label=\"" << prt << " " << mPythia->GetK(prt,1) << " " << mPythia->GetK(prt,2) << "\\n";
    cout << mPythia->GetK(prt,3) << " " << mPythia->GetK(prt,4) << " " << mPythia->GetK(prt,5) << "\\n";
    cout << mPythia->GetP(prt,4) << " " << mPythia->GetP(prt,5) << "\\n";
    cout << '"';
    cout << ",penwidth=2,color=\"";
    if (prt>2 && prt < 9)
        cout << "blue";
    else
        cout << "violet";
    cout << "\"];" << endl;

    if (prt == 7 || prt == 8) {
        if (prt==7)
            mother += 1;
        if (prt==8)
            mother -= 1;
        cout << mother << " -> " << prt << " [label=\"" << prt << " " << mPythia->GetK(prt,1) << " " << mPythia->GetK(prt,2) << "\\n";
        cout << mPythia->GetK(prt,3) << " " << mPythia->GetK(prt,4) << " " << mPythia->GetK(prt,5) << "\\n";
        cout << mPythia->GetP(prt,4) << " " << mPythia->GetP(prt,5) << "\\n";
        cout << '"';
        cout << ",penwidth=2,color=\"";
        cout << "blue";
        cout << "\"];" << endl;
    }

    mother = prt;
    prt = mPythia->GetK(prt,4);
    if (prt==mPythia->GetK(mother,5) && mPythia->GetK(prt,2)==92) {
        cout << mother << " -> " << prt << " [label=\"" << prt << " " << mPythia->GetK(prt,1) << " " << mPythia->GetK(prt,2) << "\\n";
        cout << mPythia->GetK(prt,3) << " " << mPythia->GetK(prt,4) << " " << mPythia->GetK(prt,5) << "\\n";
        cout << mPythia->GetP(prt,4) << " " << mPythia->GetP(prt,5) << "\\n";
        cout << '"';
        cout << ",penwidth=2,color=\"";
        cout << "yellow";
        cout << "\"];" << endl;
    }
}


/* Graphviz printing for a whole event */
void Pythia6Jets::PrintEvent()
{
    cout << "digraph test {" << endl;
    cout << "randir=LR;" << endl;
    cout << "ranksep=1.5;" << endl;
    cout << "node [width=0.03,height=0.03,shape=point,label=\"\"];" << endl;
    for (unsigned prt = 1; prt <= mPythia->GetN(); ++prt)
        PrintParticle(prt);
    cout << "}" << endl;
}
