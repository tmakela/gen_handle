#include "HerwigTree.h"
using namespace Herwig;
using namespace ThePEG;

/* STRUCTURES FOR STORING NECESSARY PARTICLE DATA ACCORDING TO THE EVENT TYPE: */

/* The danish number system is very complicated, so better just divide with GeV */
void HerwigTree::ParticleAdd(const tPPtr& part, char jetid)
{
    auto pdgid = part->id();
    if (pdgid==22 && GammaChecker(part)) pdgid = 20;

    mJetId.push_back(jetid);
    mPDGID.push_back(pdgid);
    mPt.push_back(part->momentum().perp()/GeV);
    mEta.push_back(part->momentum().eta());
    mPhi.push_back(part->momentum().phi());
    mE.push_back(part->momentum().e()/GeV);
} // ParticleAdd

/* The danish number system is very complicated, so better just divide with GeV */
void HerwigTree::PartonAdd(const tPPtr& part, char jetid, char tag, int ptnid, int ownid)
{
    mPJetId.push_back(jetid);
    mPPtnId.push_back(ptnid);
    mPOwnId.push_back(ownid);
    mPTag.push_back(tag);
    mPPDGID.push_back(part->id());
    mPPt.push_back(part->momentum().perp()/GeV);
    mPEta.push_back(part->momentum().eta());
    mPPhi.push_back(part->momentum().phi());
    mPE.push_back(part->momentum().e()/GeV);
    mDR.push_back(-1);
} // PartonAdd

void HerwigTree::PartonAdd(unsigned num, char jetid)
{
    assert(num < mPartonInfo.size() and ((jetid < 0) or (unsigned) jetid < mSortedJets.size()));
    mPJetId.push_back(jetid);
    mPPtnId.push_back(mPartonInfo[num].ptnid);
    mPOwnId.push_back(mPartonInfo[num].ownid);
    mPPDGID.push_back(mPartonInfo[num].id);
    mPTag.push_back(mPartonInfo[num].tag);
    mPPt.push_back(mPartonInfo[num].p4.pt());
    mPEta.push_back(mPartonInfo[num].p4.eta());
    // Uniformize phi values between mEvent and PseudoJet
    mPPhi.push_back(mPartonInfo[num].p4.phi() - (mPartonInfo[num].p4.phi()>fastjet::pi ? 2*fastjet::pi : 0));
    mPE.push_back(mPartonInfo[num].p4.e());
    mDR.push_back(jetid<0 ? -1.0 : mPartonInfo[num].p4.delta_R(mSortedJets[jetid]));
} // PartonAdd

void HerwigTree::JetAdd(unsigned jet, int spoil)
{
    mJPt.push_back(mSortedJets[jet].pt());
    mJEta.push_back(mSortedJets[jet].eta());
    mJPhi.push_back(mSortedJets[jet].phi());
    mJE.push_back(mSortedJets[jet].e());
    if (spoil==0) {
        /* Start messing around with jet constituents */
        mJetParts = sorted_by_pt(mSortedJets[jet].constituents());
        Cuts();
        mConst.push_back(mCutJetParts.size());
        mPTD.push_back(PTD());
        mSigma2.push_back(Sigma2());
    }
} // JetAdd

void HerwigTree::PartonAppend(fastjet::PseudoJet p4, tPPtr part, char tag, int ptnid)
{
    mPartonInfo.push_back(PartonHolder{p4, (int) part->id(), tag, ptnid, part->number(), false});
    p4 *= mGhostCoeff;
    p4.set_user_index(-mPartonInfo.size());
    mJetInputs.push_back(p4);
    //mPJetInputs.push_back(p4);
}


/* TRIVIAL STUFF FOR EVENT ANALYSIS */

void HerwigTree::Initialize()
{
    /* Particle lvl */
    mIsolation.clear();

    mJetId.clear();
    mPDGID.clear();
    mPt.clear();
    mEta.clear();
    mPhi.clear();
    mE.clear();
    /* Parton lvl */
    mPJetId.clear();
    mPPtnId.clear();
    mPOwnId.clear();
    mPPDGID.clear();
    mPTag.clear();
    mPPt.clear();
    mPEta.clear();
    mPPhi.clear();
    mPE.clear();
    mDR.clear();
    /* Jet lvl */
    mJPt.clear();
    mJEta.clear();
    mJPhi.clear();
    mJE.clear();
    mJPtnId.clear();
    mConst.clear();
    mPTD.clear();
    mSigma2.clear();

    mJetInputs.clear();
    //mNJetInputs.clear();
    //mPJetInputs.clear();
    mSortedJets.clear();
    mJetParts.clear();
    mCutJetParts.clear();

    mPartonInfo.clear();
    mAncestry.clear();
    mNAncestry.clear();
    mWs.clear();

    mTopLepts.clear();
    mFinals.clear();
    mGammas.clear();
    mMuons.clear();
    mChargeds.clear();

    mBNuCount = 0;
    mONuCount = 0;
    mNuOB = 0;
    mNuOC = 0;
    mNuOLept = 0;
    mNuOOther = 0;
    mNuB = 0;
    mNuC = 0;
    mNuLept = 0;
    mNuOther = 0;

    mLeptonFriend = 0;

    /* Special particle indices are saved to eliminate saving overlap. */
    mSpecialIndices.clear();

    mInfo = 0;

    mMetVect = fastjet::PseudoJet();
    mHardProcCount = 0;
    mWeight = mEvent->weight();
    mHardHandler = mEvent->primaryCollision()->handler();
} // Initialize


void HerwigTree::analyze(tEventPtr event, long ieve, int loop, int status)
{
    try {
        if (loop > 0 || status != 0 || !event) return;
        // Fancy plots:
        //event->printGraphviz();

        mEvent = event;
        Initialize();
        mTotWgt += mWeight;
        if (!HardProc()) return;

        if (!FinalState()) return;
        if (Veto()) return;

        if (!JetProcess()) return;

        mSelWgt += mWeight;
        mTree->Fill();

        if (ieve%mTimerStep==0&&ieve>0) mTimer.printTime();
    } catch (std::exception& e) {
        cout << "An error occurred: " << e.what() << endl;
    }
} // analyze


bool HerwigTree::Veto()
{
    vector<pair<fastjet::PseudoJet,tPPtr>> dummy;
    vector<double> leptonDR;

    if (mMode==2) {
        dummy = mGammas;
        for (auto i = 0u; i < dummy.size(); ++i)
            leptonDR.push_back(mGammaDR);
    } else if (mMode==3) {
        dummy = mMuons;
        for (auto i = 0u; i < dummy.size(); ++i)
            leptonDR.push_back(mMuDR);
    } else if (mMode>=4) {
        dummy = mChargeds;
        for (auto i = 0u; i < dummy.size(); ++i) {
            if (abs(dummy[i].second->id())==11)
                leptonDR.push_back(mElDR);
            else
                leptonDR.push_back(mMuDR);
        }
    } else {
        return false;
    }

    vector<double> E_dR(dummy.size(),0);

    for (auto &part : mJetInputs) {
        if (part.user_index()<0)
            continue;
        for (unsigned i = 0; i < dummy.size(); ++i) {
            double dR = dummy[i].first.delta_R(part);
            if (dR < leptonDR[i])
                E_dR[i] += part.e();
        }
    }

    for (unsigned i = 0; i < dummy.size(); ++i) {
        float isolation = E_dR[i]/dummy[i].first.e();
        mIsolation.push_back(isolation);
        PartonAdd(dummy[i].second,-1,6);
    }

    return false;
} // Veto


bool HerwigTree::FinalState()
{
    /* Final state particles */
    // All particles:
    //  tPVector all;
    //  event->select(std::back_inserter(all),SelectAll());
    //  all.begin(), all.end() etc.
    if (mMode>=4) {
        tPVector all;
        mEvent->select(std::back_inserter(all),SelectAll());
        for (auto &part : all) {
            auto id = abs(part->id());
            if (IsHadron(id) and IsExcitedHadronState(part,5)==0)
                PartonAppend(PseudoJettify(part),part,(char)9,-1);
        }
    }

    for (auto &part : mEvent->getFinalState()) {
        auto finalIdx = part->number();
        auto prt = PseudoJettify(part);
        prt.set_user_index(mFinals.size());
        mFinals.push_back(part);

        int id = abs(part->id());

        if (mGammaCase and prt.perp()>mGammaPt)
            mGammas.push_back(std::make_pair(prt,part));
        else if (mZCase and prt.perp()>mMuPt)
            mMuons.push_back(std::make_pair(prt,part));

        /* Do not add neutrinos to "observed" particles. */
        if (id==12||id==14||id==16) {
            mMetVect += prt;
            if (mMode>=4 and Absent(finalIdx)) {
                auto parent = part->parents()[0];
                while (abs(parent->id())==24)
                    parent = parent->parents()[0];
                auto feeling = abs(parent->id())/100;
                if (feeling>9)
                    feeling /= 10;
                if (feeling==5) ++mNuOB;
                else if (feeling==4) ++mNuOC;
                else if (IsLepton(abs(parent->id()))) ++mNuOLept;
                else ++mNuOOther;
                PartonAdd(part,-1,8);
            }
        } else if (Absent(finalIdx))
            mJetInputs.push_back(prt);

        if (mMode>=4) {
            if(Absent(part->number()) and (id==11 or id==13 or id==15) and prt.perp()>mLeptPt)
                mChargeds.push_back(std::make_pair(prt,part));
        }
    }

    return true;
} // FinalState


bool HerwigTree::JetProcess()
{
    if (mJetDef.jet_algorithm() == fastjet::undefined_jet_algorithm)
        mJetDef = fastjet::JetDefinition(fastjet::antikt_algorithm, 0.4, fastjet::E_scheme, fastjet::Best);
    fastjet::ClusterSequence clustSeq(mJetInputs, mJetDef);
    vector<fastjet::PseudoJet> unsorted = clustSeq.inclusive_jets(4.0);
    mSortedJets = sorted_by_pt(unsorted);
    for (unsigned jet = 0; jet<mSortedJets.size(); ++jet) {
        JetAdd(jet);

        for (auto &part : mJetParts) {
            int prt = part.user_index();
            if (prt<0) {
                /* Adding uncorrected and corrected parton momentum */
                prt *= -1;
                --prt;
                PartonAdd(prt,jet);
                mPartonInfo[prt].used = true;
            } else {
                //ParticleAdd(prt,jet);
                continue;
            }
        }
    }

    for (unsigned prt = 0; prt < mPartonInfo.size(); ++prt) {
        if (mPartonInfo[prt].used)
            continue;
        PartonAdd(prt,-1);
    }

    return true;
} // JetProcess


/* HARD PROCESS ANALYSIS (NONTRIVIAL) */

bool HerwigTree::HardProc()
{

    //tcParticleSet partos;
    //event->select(inserter(partos), ThePEG::ParticleSelector<TTBar>());
    //for (auto it = partos.begin(); it != partos.end(); ++it)
    //    cout << "juu " << (*it)->number() << " " << (*it)->id() << endl;

    /* The hardest subprocess */
    // Hard process (method 1 - brings also the id 82 collimations)
    //  const ParticleVector hardParts = event->primarySubProcess()->outgoing(); (const_iterator)
    // Hard process (method 2 - preferred)
    //  tPVector hardParts = event->primaryCollision()->step(0)->getFinalState(); (const_iterator)
    tPVector hardParts = mEvent->primaryCollision()->step(0)->getFinalState();
    // for (auto &part : hardParts) {
    //     Iter_print(part, 0);
    // }
    float minPt = 100000;
    int bCount = 0, qCount = 0;
    for (auto &hard : hardParts) {
        int absId = abs(hard->id());

        if (absId==82) continue;
        auto part = OptimalParton(hard);

        mGammaCase = (mMode==2 && absId==ParticleID::gamma);
        mZCase = (mMode==3 && absId==ParticleID::muminus);
        mTopCase = (mMode>=4 and absId==6);

        //Lorentz5Momentum first, second;
        //SMMomenta(first, *part);
        //AltMomenta(second, *part);

        //cout << "id: " << absId << ": " << hard->number() << ": " << hard->momentum().perp()/GeV << endl;
        //cout << absId << ": " << hard->momentum().perp()/GeV << " " << first.perp()/GeV << " " << second.perp()/GeV << endl;
        minPt = min(minPt,part->momentum().perp()/GeV);

        if (mGammaCase) {
            if (!GammaAdd(part))
                return false;
        } else if (mZCase) {
            if (!MuonAdd(part))
                return false;
        } else if (mTopCase) {
            auto topc = Probe(part);
            int wInd = -1;
            for (auto &child : topc) {
                auto childId = abs(child->id());
                if (childId > 10 and childId < 20) {
                    mTopLepts.push_back(child);
                } else if (childId==24) {
                    PartonAppend(PseudoJettify(child),child,(char) 2,-1);
                    wInd = child->number();
                    if (part->children().size()>0 and IsLepton(part->children()[0]->id()))
                        mLeptonFriend = part->number();
                } else if (childId==5) {
                    ++bCount;
                    PartonAppend(PseudoJettify(child),child,(char) 0,-1);
                    PartonDescend(child);
                    BNeutrinos(child);
                } else if (childId<5) {
                    ++qCount;
                    if (wInd < 0)
                        wInd = part->number();
                    PartonAppend(PseudoJettify(child),child,(char) 0,wInd);
                    PartonDescend(child);
                } else if (childId==22) {
                    throw runtime_error("Illegal top photon activity!");
                } else if (childId==21) { // This is odd
                    PartonAppend(PseudoJettify(child),child,(char) 0,-1);
                    PartonDescend(child);
                } else {
                    throw runtime_error(Form("Unexpected t consequences! %ld",childId));
                }
            }

            if (wInd==part->number())
                throw runtime_error("W lost. :(");
        } else if (absId<10 || absId==21) {
            PartonAppend(PseudoJettify(part),part,(char) 0,-1);
            PartonDescend(part);
        } else {
            cerr << "Unidentified ME particle." << endl;
            continue;
        }
        ++mHardProcCount;
    }
    mPtHat = minPt;

    for (auto &tl : mTopLepts)
        if (!LeptonAdd(tl,mLeptonFriend))
            return false;

    /* ttbar events: seek lepton+jets (one w to leptons, one to quarks) */
    if (mMode>=4 and (bCount!=2 or qCount!=2 or mTopLepts.size()!=2)) {
        if (!generator()->repeatEvent())
            throw runtime_error("Repeating events does not work.");
        AddMessage("Non-semileptonic ttbar",mWeight);
        return false;
    }

    if (mMode==3) --mHardProcCount;

    /* Sanity checks */
    if (   (mMode<4 && mHardProcCount !=2)
        or (mMode==2 && mSpecialIndices.size()!=1)
        or (mMode==3 && mSpecialIndices.size()!=2)
        or (mMode>=4 && mHardProcCount != 2) )
    {
        throw std::logic_error("Unexpected hard process structure");
    }

    return true;
}


bool HerwigTree::GammaAdd(tPPtr gamma)
{
    gamma = OptimalParton(gamma);
    /* No pair production */
    if (gamma->children().size()>0 or gamma->decayed()) {
        if (!generator()->repeatEvent()) throw runtime_error("Repeating events does not work.");
        AddMessage("Signal photon pair production.",mWeight);
        return false;
    }
    mSpecialIndices.push_back(gamma->number());
    PartonAdd(gamma,-1,(char) 2);
    return true;
}


bool HerwigTree::MuonAdd(tPPtr muon)
{
    muon = OptimalParton(muon);
    while (muon->decayed() > 0 or muon->children().size()>0) {
        for (auto &mu : muon->children()) {
            if (abs(mu->id())==ParticleID::muminus) {
                muon = mu;
                continue;
            }
        }
    }
    mSpecialIndices.push_back(muon->number());
    PartonAdd(muon,-1,(char) 2);
    return true;
}


bool HerwigTree::LeptonAdd(tPPtr lepton, int parent)
{
    int type = abs(lepton->id())%2;
    if (type) {
        /* Charged leptons */

        /* Save a raw charged lepton */
        PartonAdd(lepton, (char)-1, '\2',parent, lepton->number());

        /* Check for tau */
        if (abs(lepton->id())==15)
            mInfo |= 1;

        while (lepton->decayed() or lepton->children().size()>0) {
            bool hadrons = false;
            auto previdx = lepton->number();
            for (auto &child : lepton->children()) {
                int absId = abs(child->id());
                if (IsHadron(absId)) {
                    hadrons = true;
                    break;
                } else if (absId==22) {
                    auto optchild = OptimalParton(child);
                    if (optchild->momentum().perp()/GeV>0.1)
                        PartonAdd(OptimalParton(child),-1,'\2', parent, optchild->number());
                } else if (IsLepton(absId)) {
                    if (absId%2==1) {
                        lepton = child;
                        hadrons = false;
                    } else {
                        /* Neutrino sent into the neutrino loop */
                        LeptonAdd(child, mLeptonFriend);
                    }
                } else if (absId==24) {
                    lepton = child;
                } else {
                    cerr << "Unexpected lepton-loop result: " << absId << " " << lepton->id() << endl;
                }
            }

            if (hadrons) {
                /* This occurs around 25-30% of the time originating from tau decay */
                AddMessage("Partonic tau decay!",mWeight);
                return false;
            } else if (previdx == lepton->number()) {
                /* Check if stuck in a loop*/
                AddMessage("Lepton loop stuck!",mWeight);
                return false;
            }
        }

        mChargeds.push_back(std::make_pair(PseudoJettify(lepton),lepton));
    } else {
        /* Neutrinos */
        lepton = OptimalParton(lepton);
        if (lepton->decayed() or lepton->children().size()>0)
            throw std::logic_error("Neutrino decay spotted.");
    }

    mSpecialIndices.push_back(lepton->number());
    PartonAdd(lepton,-1,(char) 3, parent,lepton->number());
    return true;
}


vector<tPPtr> HerwigTree::Probe(tPPtr part, int lvl) const {
    vector<tPPtr> hards;
    for (auto &child : part->children()) {
        auto absId = abs(child->id());
        auto oChild = OptimalParton(child);
//         if (absId==22) {
//             cout << part->id() << endl;
//             for (auto &port : part->children())
//                 cout << "  " << port->id() << endl;
//         }
        if (absId==6 or absId==24) {
            auto results = Probe(oChild,lvl+1);
            hards.reserve(hards.size() + results.size());
            if (absId==24)
                hards.push_back(oChild);
            hards.insert(hards.end(), results.begin(), results.end());
        } else if (absId==22 and abs(part->id())==6) {
            continue;
        } else
            hards.push_back(oChild);
    }
    return hards;
} // Probe


void Herwig::HerwigTree::BNeutrinos(const tPPtr& part)
{
    if(part->children().empty() and !part->decayed()) {
        auto absId = abs(part->id());
        if (absId!=12 and absId!=14 and absId!=16) return;
        if (part->parents().size()==0)
            throw runtime_error("No parents!!");
        auto parent = part->parents()[0];
        while (abs(parent->id())==24)
            parent = parent->parents()[0];
        auto feeling = abs(parent->id())/100;
        while (feeling>9)
            feeling /= 10;
        if (feeling==5) ++mNuB;
        else if (feeling==4) ++mNuC;
        else if (IsLepton(abs(parent->id()))) ++mNuLept;
        else ++mNuOther;
        ++mBNuCount;
        PartonAdd(part,-1,7);
        mSpecialIndices.push_back(part->number());
    } else {
        for (auto &child : part->children())
            BNeutrinos(child);
    }
} // BNeutrinos


void HerwigTree::PartonDescend(tPPtr part)
{
    for (auto &dtr : part->children()) {
        auto truth = OptimalParton(dtr);
        if (part->momentum().perp()/GeV<4)
            continue;
        auto absId = abs(truth->id());
        if (IsParton(absId)) {
            PartonAppend(PseudoJettify(truth),truth,4,part->number());
            PartonDescend(truth);
        }
    }
}


tPPtr HerwigTree::OptimalParton(tPPtr part) const
{
    while (part->children().size()==1) {
        auto trial = (part->children())[0];
        auto absId = abs(trial->id());
        if (trial->parents().size()>1 or absId>100 or absId==81)
            return part;
        part = trial;
    }

    if (part->children().size()==0 and !IsLepton(abs(part->id())) and part->id()!=22)
        cerr << "No partonic children?" << endl;

    return part;
}

Lorentz5Momentum HerwigTree::FinalMomenta(tPPtr parent) const {
    Lorentz5Momentum dummy;
    if(parent->children().empty()) {
        return parent->momentum();
    } else {
        for (auto &child : parent->children())
            dummy += FinalMomenta(child);
    }
    return dummy;
}

Lorentz5Momentum HerwigTree::FinalPMomenta(tPPtr parent) const {
    if(parent->children().empty()) {
        return parent->momentum();
    }
    for (auto& child : parent->children()) {
        auto absId = abs(child->id());
        if (absId>100 || absId==81)
            return parent->momentum();
    }

    Lorentz5Momentum dummy;
    for (auto &child : parent->children())
        dummy += FinalPMomenta(child);
    return dummy;
}




/* JET PROPERTIES */

double HerwigTree::PTD()
{
    double square = 0, linear = 0;
    for(size_t q = 0; q != mCutJetParts.size(); ++q) {
        square += pow(mCutJetParts[q].pt(),2);
        linear += mCutJetParts[q].pt();
    }
    return sqrt(square)/linear;
}

double HerwigTree::Sigma2()
{
    double weightedDiffs[4] = {0,0,0,0};
    double phi = 0, eta = 0, pT2 = 0;

    for(size_t q = 0; q != mCutJetParts.size(); ++q) {
        pT2 += pow(mCutJetParts[q].pt(),2);
        eta += pow(mCutJetParts[q].pt(),2)*mCutJetParts[q].eta();
        phi += pow(mCutJetParts[q].pt(),2)*mCutJetParts[q].phi();
    }
    eta /= pT2; phi /= pT2;

    for(unsigned int q = 0; q != mCutJetParts.size(); ++q)
    {
        double deltaEta = eta-mCutJetParts[q].eta();
        double deltaPhi = TVector2::Phi_mpi_pi( phi-mCutJetParts[q].phi() );
        double pT2Tmp = pow(mCutJetParts[q].pt(),2);
        weightedDiffs[0] += pT2Tmp*deltaEta*deltaEta;
        weightedDiffs[3] += pT2Tmp*deltaPhi*deltaPhi;
        weightedDiffs[1] -= pT2Tmp*deltaEta*deltaPhi;
    }
    weightedDiffs[2] = weightedDiffs[1];

    TMatrixDSymEigen me( TMatrixDSym(2,weightedDiffs) );
    TVectorD eigenvals = me.GetEigenValues();

    return sqrt(eigenvals[1]/pT2);
}

void HerwigTree::Cuts()
{
    mCutJetParts.clear();

    if (mCutMode) {
        /* Explicit cuts (pt cut for photons and neutral hadrons) */
        vector<fastjet::PseudoJet> tmpParts;
        for (auto &q : mJetParts) {
            if (q.user_index() < 0) continue;
            int id = abs(mFinals[q.user_index()]->id());
            if (q.pt()>1 or (id != 22 and (!IsHadron(id) or IsCharged(id))))
                tmpParts.push_back(q);
        }

        /* Implicit cuts (pt cut for hadrons - harsher for neutral hadrons) */
        for (auto &q : tmpParts) {
            int id = abs(mFinals[q.user_index()]->id());
            if (!IsHadron(id) or ((IsCharged(id) and q.pt()>0.3) || (!IsCharged(id) and q.pt()>3)))
                mCutJetParts.push_back( q );
        }
    } else {
        for (auto &q : mJetParts) {
            if (q.user_index() < 0)
                continue;
            mCutJetParts.push_back(q);
        }
    }
}

bool HerwigTree::IsHadron(int pdg) const
{
    if(pdg>99) return true;
    return false;
}

bool HerwigTree::IsParton(int pdg) const
{
    if(pdg<=6 or pdg==21) return true;
    return false;
}

bool HerwigTree::IsLepton(int pdg) const
{
    if (pdg>10 and pdg<20) return true;
    return false;
}

bool HerwigTree::IsCharged(int pdg) const
{
    int charge = 0;
    /* photons and neutrinos */
    if (pdg==22 || pdg==12 || pdg==14 ||pdg==16 ) return false;
    /* charged leptons */
    if (pdg==11 || pdg==13 || pdg==15 ) return true;

    pdg = (pdg/10)%1000;
    if (pdg < 100) { /* Mesons */
        if ((pdg%10)%2 == 0) { charge += 2; }
        else { charge -= 1; }

        if ((pdg/10)%2 == 0) { charge -= 2; }
        else { charge += 1; }

    } else { /* Baryons */
        while (pdg != 0) {
            int digit = pdg%10;
            pdg = pdg/10;
            if (digit%2 == 0) { charge += 2; }
            else { charge -= 1; }
        }
    }
    if (charge == 0) return false;
    else return true;
}





/* VARIOUS HELPER FUNCTIONALITIES */

bool HerwigTree::GammaChecker(const tPPtr& photon) const {
    /* One mother, which is pi0 */
    auto parents = photon->parents();
    if (parents.size()!=1 || parents[0]->id()!=ParticleID::pi0) return false;

    auto children = parents[0]->children();
    if (children.size()!=2) return false;

    double eDifference = fabs((parents[0]->momentum().t()-children[0]->momentum().t()-children[1]->momentum().t() )/GeV);
    if (eDifference > 0.001) return false;

    return true;
}

int HerwigTree::IsExcitedHadronState(const tPPtr& part, int quarkId) const
{
    assert(quarkId>=0 && quarkId<=5);
    if (!HadrFuncs::StatusCheck(quarkId, part->id()))
        return -1;

    ParticleVector children = part->children();
    for (auto &child : part->children())
        if (HadrFuncs::StatusCheck(quarkId, child->id())) return 1;
        return 0;
}

int HerwigTree::GetStatusCode(const tPPtr& part) const
{
    int status = 1;
    if (!part->children().empty() || part->next()) {
        tStepPtr step = part->birthStep();
        if ((!step || (step && (!step->handler() || step->handler() == mHardHandler))) && part->id() != 82)
            status = 3;
        else
            status = 2;
    }
    return status;
}

void HerwigTree::Print_parents(const tPPtr& part) const {
    cout << " " << part->number() << " " << part->id() << "  ";
    for (auto &parent : part->parents())
        cout << parent->number() << "-" << GetStatusCode(parent) << " ";
    cout << endl;
    for (auto &parent : part->parents())
        Print_parents(parent);
}

void HerwigTree::Iter_print(tPPtr prt, unsigned gen) const {
    cout << gen++ << " " << prt->id() << endl;
    const ParticleVector childs = prt->children();
    for (ParticleVector::const_iterator part = childs.begin(); part != childs.end(); ++part)
        Iter_print(*part, gen);
}

inline bool HerwigTree::Absent(unsigned int num) const
{
    return std::find(mSpecialIndices.begin(),mSpecialIndices.end(),num)==mSpecialIndices.end();
}

inline void HerwigTree::AddMessage(string msg, double wgt) {
    if (mErrorList.count(msg)==0)
        mErrorList[msg] = wgt;
    else
        mErrorList[msg] += wgt;
}

/* A very handy handle */
inline TLorentzVector HerwigTree::TLorentzify(const tPPtr& part) const
{
    return TLorentzVector(part->momentum().x()/GeV,part->momentum().y()/GeV,part->momentum().z()/GeV,part->momentum().t()/GeV);
}

/* Another handy handle */
inline fastjet::PseudoJet HerwigTree::PseudoJettify(const tPPtr& part) const
{
    return fastjet::PseudoJet(part->momentum().x()/GeV,part->momentum().y()/GeV,part->momentum().z()/GeV,part->momentum().t()/GeV);
}

inline fastjet::PseudoJet HerwigTree::PseudoJettify(TLorentzVector p4) const {
    return fastjet::PseudoJet(p4.Px(),p4.Py(),p4.Pz(),p4.E());
}
