var indexSectionsWithContent =
{
  0: "acdefghijlmnopstuv",
  1: "hpt",
  2: "hst",
  3: "hs",
  4: "acdfghijlmpstv",
  5: "efhimnopstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

