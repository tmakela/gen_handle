var searchData=
[
  ['particleadd',['ParticleAdd',['../class_herwig_1_1_herwig_tree.html#a8af0194638699f4ae3673f4c9913e8f8',1,'Herwig::HerwigTree']]],
  ['partonadd',['PartonAdd',['../class_herwig_1_1_herwig_tree.html#a43d06e27673fbf2738ad09b91433c23b',1,'Herwig::HerwigTree::PartonAdd(const tPPtr &amp;part, char jetid, char tag, int ptnid=0, int ownid=0)'],['../class_herwig_1_1_herwig_tree.html#a4aa2bfe3825370d30304afee49896c7a',1,'Herwig::HerwigTree::PartonAdd(unsigned num, char jetid)']]],
  ['print_5fparents',['Print_parents',['../class_herwig_1_1_herwig_tree.html#a1879d8a4e4b59c7ce1ca8d667bc2ee75',1,'Herwig::HerwigTree']]],
  ['pseudojettify',['PseudoJettify',['../class_herwig_1_1_herwig_tree.html#a9d29816b401eaf3746db1c91f8d03549',1,'Herwig::HerwigTree::PseudoJettify(const tPPtr &amp;part) const '],['../class_herwig_1_1_herwig_tree.html#a8941cb1d95ce44bad9f88e24004b666b',1,'Herwig::HerwigTree::PseudoJettify(TLorentzVector p4) const ']]],
  ['ptd',['PTD',['../class_herwig_1_1_herwig_tree.html#a153d67d1f7a9e992906e7159636d5f2b',1,'Herwig::HerwigTree']]]
];
